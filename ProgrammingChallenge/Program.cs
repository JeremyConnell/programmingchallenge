﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace ProgrammingChallenge
{
    //Notes:
    //* Using live data for tests (todo: use an offline copy)
    //* Using list classes to reuse common logic for indexing/sorting/aggregation etc
    //* No 3rd-party components (strictly framework only)
    //* Dependency Injection gets messy and intrusive, is overkill here (I know how to, but don't like the implementations I've seen)
    //* Setting up unit testing framework is also overkill (Is typically already set up, and most frameworks are remarkably similar to use)

    class Program
    {
        #region Constants
        const string URL = "http://agl-developer-test.azurewebsites.net/people.json";
        const int ATTEMPTS = 10;
        #endregion


        #region Main Logic
        static void Main(string[] args)
        {
            //Download & Deserialise
            //* Trivial checks to exit early
            var owners = GetOwners();
            if (null == owners)
            {
                Console.ReadLine();
                return;
            }

            //Presentation 
            //*Nailing this part with a short (but highly-descriptive) syntax is decisively important
            foreach (var i in owners.Genders)
            {
                Console.WriteLine(i);
                foreach (var j in owners.GetByGender(i).Pets)
                    Console.WriteLine($"\t{j.Name}");
            }
            Console.WriteLine();


            //Regression Tests 
            //Not a feature of my usual agile, rapid development (RAD) style (economic/business tradeoff); but essential for in-production code
            //Once handed over to a care-taker type developer, at the end of major development, unit-tests for africa will make future deployments safer (comparatively low-value)
            //Regression tests are inherently more useful at detection, which is more important, as a good dev can isolate the problem relatively quickly (comprehensive unit tests make it even easier, at some cost)
            
            //1. Check # of Genders
            if (2 != owners.Genders.Count)
                Console.WriteLine("TEST FAILED: Should be 2 genders, found " + owners.Genders.Count);

            //2. Check # of Male owners
            var male = owners.GetByGender("Male");
            if (male.Count != 3)
                Console.WriteLine("TEST FAILED: Should be 3 male owners, found " + male.Count);

            //3. Check # of Pets for Male owners
            if (male.Pets.Count != 6)
                Console.WriteLine("TEST FAILED: Male owners should have 6 pets, found " + male.Pets.Count);

            //4. Check first pet names for male
            var firstMalePet = male.Pets[0].Name;
            if (firstMalePet != "Fido")
                Console.WriteLine("TEST FAILED: First Male pet should be '', found '" + firstMalePet);

            //5. Check last pet name for male
            var lastMalePet = male.Pets.Last().Name;
            if (lastMalePet != "Tom")
                Console.WriteLine("TEST FAILED: First Male pet should be '', found '" + lastMalePet);

            //Summary
            Console.WriteLine("Ran 5 Tests");
            Console.ReadLine();
        }
        #endregion


        #region Download & Deserialise
        static COwnerList GetOwners()
        {
            //Download
            var json = GetJson();   //Todo: return stream for better scalability
            if (null == json)
            {
                Console.WriteLine($"Gave up after {ATTEMPTS} attempts");
                return null;
            }

            //Deserialise
            var jss = new DataContractJsonSerializer(typeof(COwnerList));
            var ms = new MemoryStream(json);
            try
            {
                return (COwnerList)jss.ReadObject(ms);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Deserialisation error: " + ex.Message);
                return null;
            }
        }

        //Download
        static byte[] GetJson()
        {
            return GetJson(new WebClient(), 0);
        }
        static byte[] GetJson(WebClient wc, int attempts)
        {
            //Limit attempts
            if (attempts > ATTEMPTS)
                return null;

            //Download URL
            try
            {
                return wc.DownloadData(URL);
            }
            catch (Exception ex)
            {
                //Sleep 5 seconds between attempts
                attempts++;
                Console.WriteLine(string.Concat("Attempt ", attempts, " of 10 - Error downloading JSON: ", ex.Message));
                System.Threading.Thread.Sleep(5 * 1000);
            }

            //Recursion
            return GetJson(wc, attempts);
        }
        #endregion


    }
}
