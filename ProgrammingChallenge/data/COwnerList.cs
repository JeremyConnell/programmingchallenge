﻿using System.Collections.Generic;
using System.Linq;

namespace ProgrammingChallenge
{
    //If comments are auto-generated, like that xml crap for documentation, then its worthless, and add clutter (negative value)
    //If a documentation-generating tool can't use reflection for that sort of thing, then its crap (personal viewpoint, shared by very few others)
    public class COwnerList : List<COwner>
    {
        #region Constructors
        //I always include these 3 constructors for a custom list
        //Normally auto-gen (for each table/entity) using gold-plated templates (at zero marginal cost)
        public COwnerList()                     : base()     { }
        public COwnerList(int size)             : base(size) { }
        public COwnerList(IList<COwner> list)   : base(list) { }
        #endregion

        #region Aggregation (Children of Many Parents)
        //Strict use of Regions gets imposed when using my code-gen approach
        //Same region names, in the same order, for a certain type of class, makes it easy to find stuff (predictable location

        private CPetList _pets;
        public CPetList Pets
        {
            get
            {
                if (null == _pets)  //Longer syntax than LINQ, but cached for performace, transparent, and reusable (personal preference)
                {
                    var temp = new CPetList();
                    foreach (var i in this)
                        if (null != i.Pets)
                            temp.AddRange(i.Pets);

                    //Sort can be seperated, by I'm in the habit of including it in an aggregation method, 
                    // because more often that not (pretty much always), you want it sorted, usually with a fairly obvious choice of property to default-sort-by
                    _pets = temp.SortByName();
                }
                return _pets; 
            }
        }
        #endregion

        #region Indexing
        //My code-generator auto-generates this sort of thing for Foreign Keys
        public List<string> Genders
        {
            get
            {
                return IndexByGender.Keys.ToList();
            }
        }
        public COwnerList GetByGender(string gender)
        {
            COwnerList temp = null;
            if (!IndexByGender.TryGetValue(gender, out temp))
            {
                temp = new COwnerList();
                IndexByGender[gender] = temp;   //Accumulates for unique input values
            }
            return temp;
        }
        private Dictionary<string, COwnerList> _indexByGender;
        public Dictionary<string, COwnerList> IndexByGender
        {
            get
            {
                if (null == _indexByGender)
                {
                    //Note: temp var is used for threadsafety, but its a rather low-brow technique
                    //Alternative lock (this) is less wasteful, particularly if lots hit it at once
                    //I only use this pattern when I know that scenario wont happen, its simple and readable
                    var temp = new Dictionary<string, COwnerList>(2);
                    COwnerList list = null;
                    foreach (COwner i in this)
                    {
                        if (!temp.TryGetValue(i.Gender, out list))
                        {
                            list = new COwnerList();
                            temp.Add(i.Gender, list);
                        }
                        list.Add(i);
                    }

                    _indexByGender = temp;
                }
                return _indexByGender;
            }
        }
        #endregion
    }
}
