﻿using System.Collections.Generic;
using System.Linq;

namespace ProgrammingChallenge
{
    //See COwnerList for more comments
    //Note the cameo use of LINQ to confirm that I can
    //Have a personal preference for explicit old-style code I can step through; well-organised, with a view to promoting reuse
    public class CPetList : List<CPet>
    {
        //Constructors
        public CPetList()                   : base()     { }
        public CPetList(int size)           : base(size) { }
        public CPetList(IList<CPet> list)   : base(list) { }

        //Sorting
        public CPetList SortByName()
        {
            return new CPetList(this.OrderBy(x => x.Name).ToList());
        }
    }
}
