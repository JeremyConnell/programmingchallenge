﻿using System.Runtime.Serialization;

namespace ProgrammingChallenge
{
    [DataContract]
    public class COwner
    {
        [DataMember(Name = "name")]     public string     Name;
        [DataMember(Name = "gender")]   public string     Gender;
        [DataMember(Name = "age")]      public int        Age;
        [DataMember(Name = "pets")]     public CPetList   Pets;
    }
}
