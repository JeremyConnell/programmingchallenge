﻿using System.Runtime.Serialization;

namespace ProgrammingChallenge
{
    [DataContract]  //More reusable that a json-only solution (dictionaries) eg. can serialse into many other formats
    public class CPet
    {
        [DataMember(Name = "name")] public string Name;
        [DataMember(Name = "type")] public string Type;
    }
}
